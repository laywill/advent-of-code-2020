# Advent Of Code 2020


[![pipeline status](https://gitlab.com/laywill/advent-of-code-2020/badges/master/pipeline.svg)](https://gitlab.com/laywill/advent-of-code-2020/-/commits/master)

Advent of Code is an annual Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

https://adventofcode.com/2020/



## Setup and Use


### Python Version

This has been developed using Python 3.9


### Create a virtual environment

On macOS and Linux:
```
python3 -m venv venv
```

On Windows:
```
python -m venv venv
```


### Activate virtual environment

On macOS and Linux:
```
source venv/bin/activate
```

On Windows:
```
.\venv\Scripts\activate
```


### Install packages from requirements

Tell pip to install all of the packages in this file using the -r flag:
```
pip install -r requirements.txt
```


### Install and configure git pre-commit

To install git hooks in your .git/ directory, execute:
```bash
pre-commit install
```


### Running checks on repsitory

If you want to run the checks on-demand (outside of git hooks), run:
```bash
pre-commit run --all-files --verbose
```


### Running Scripts

All scripts assume you are running them from the root of the repsitory.
Scripts do not take any parameters, and are invoked directly e.g.:
```
python ./src/day1.py
```


### Leaving the virtual environment

Simply type:
```
deactivate
```



## Development

### Updating requirements

To update `requirements.txt` use the following command:
```
pip freeze > requirements.txt
```
