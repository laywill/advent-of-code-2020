#!/bin/bash
# shellcheck disable=SC2061
for FILE in $(find ./src -name day*.py | sort)
do
    echo -e "\nRunning $FILE"
    time python "$FILE"
done
