
#!/bin/bash
# shellcheck disable=SC2061
for FILE in $(find ./ -name *.sh | sort)
do
    echo -e "\nLinting $FILE with Shellcheck"
    shellcheck "$FILE"
done
