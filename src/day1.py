#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day1_input_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day1_input.txt"

    # Read the files and convert to lists of ints
    expenses_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST, "\n")
    expenses_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_DATA, "\n")

    expenses_test = aoc_file_io.list_of_str_to_list_of_int(expenses_test)
    expenses_real = aoc_file_io.list_of_str_to_list_of_int(expenses_real)

    # Part 1
    # - Find TWO entries that sum to 2020
    # - Multiply the TWO numbers together
    # Answer: Total fuel required.
    result_test = -1
    for x in expenses_test:
        counterpart = 2020 - x
        if counterpart in expenses_test:
            result_test = x * counterpart
            break
    print("Part 1 - Test:", result_test)

    result_real = -1
    for x in expenses_real:
        counterpart = 2020 - x
        if counterpart in expenses_real:
            result_real = x * counterpart
            break
    print("Part 1 - Data:", result_real)

    # Part 2
    # - Find THREE entries that sum to 2020
    # - Multiply the THREE numbers together
    result_test = -1
    for i1 in range(len(expenses_test)):
        # Iterating through all elements in the list
        for i2 in range(len(expenses_test) - i1):
            # Iterating through elements of the list starting at the next index in the list
            for i3 in range(len(expenses_test) - i2):
                # Iterating through elements of the list starting at the next index in the list
                if 2020 == expenses_test[i1] + expenses_test[i2] + expenses_test[i3]:
                    result_test = (
                        expenses_test[i1] * expenses_test[i2] * expenses_test[i3]
                    )
                    break
    print("Part 2 - Test:", result_test)

    result_real = -1
    # Iterate through all elements in the list
    for i1 in range(len(expenses_real)):
        # Iterating through all elements in the list
        for i2 in range(len(expenses_real) - i1):
            # Iterating through elements of the list starting at the next index in the list
            for i3 in range(len(expenses_real) - i2):
                # Iterating through elements of the list starting at the next index in the list
                if 2020 == expenses_real[i1] + expenses_real[i2] + expenses_real[i3]:
                    result_real = (
                        expenses_real[i1] * expenses_real[i2] * expenses_real[i3]
                    )
                    break
    print("Part 2 - Real:", result_real)
