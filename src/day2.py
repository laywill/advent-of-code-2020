#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


def valid_password_count(password_and_policy) -> bool:
    """
    For a given password and poliy taking the form:
    min-max x: password_string
    Return true if the character x appears in the password_string
    At least min times, and no more than max times.
    """
    pwd_parts = password_and_policy.split(" ")
    idx_lo, idx_hi = pwd_parts[0].split("-")
    appearances = pwd_parts[2].count(pwd_parts[1][0])

    # Convert from srings to integers
    idx_lo = int(idx_lo)
    idx_hi = int(idx_hi)

    # If the password meets the policy return true
    # ...otherwise return True
    return (appearances <= idx_hi) and (appearances >= idx_lo)


def valid_password_index(password_and_policy) -> bool:
    """
    For a given password and poliy taking the form:
    index1-index2 x: password_string
    Return true if the character x appears in the password_string
    at index1 OR index2 (index from 1), not both!
    """
    pwd_parts = password_and_policy.split(" ")
    idx_lo, idx_hi = pwd_parts[0].split("-")
    char = pwd_parts[1][0]

    # Convert 1 indexed to 0 indexed
    idx_lo = int(idx_lo) - 1
    idx_hi = int(idx_hi) - 1

    # If the password meets the policy return true
    # ...otherwise return True
    return (pwd_parts[2][idx_lo] == char) ^ (pwd_parts[2][idx_hi] == char)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day2_input_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day2_input.txt"

    # Read the files and convert to lists of strings
    passwords_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST, "\n")
    passwords_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_DATA, "\n")

    # Part 1
    # Policy indicates lowest and highest number of times a given letter must appear
    # e.g. 1-3 a: abcde
    # - contains 1x 'a'
    # - Policy requires min 1x 'a', max 3x 'a'
    # - This password is valid
    # Answer: How many passwords are valid
    result_test = 0
    for x in passwords_test:
        if valid_password_count(x):
            result_test += 1
    print("Part 1 - Test:", result_test)

    result_real = 0
    for x in passwords_real:
        if valid_password_count(x):
            result_real += 1
    print("Part 1 - Real:", result_real)

    # Part 2
    # Policy indicates two indexes a given letter must appear in
    # Index begins with 1, not 0
    # e.g. 1-3 a: abcde
    # - contains 'a' at index 1
    # - Policy requires 'a' at index 1 or 3
    # - This password is valid
    # Answer: How many passwords are valid
    result_test = 0
    for x in passwords_test:
        if valid_password_index(x):
            result_test += 1
    print("Part 2 - Test:", result_test)

    result_real = 0
    for x in passwords_real:
        if valid_password_index(x):
            result_real += 1
    print("Part 2 - Test:", result_real)
