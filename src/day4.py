#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
import re
from pathlib import Path

import aoc_file_io


def this_passport_valid(passport_input_str) -> bool:
    """
    Test whether all required fields are in this string.
    Return true or false
    """
    this_passport_valid = True
    if "byr" not in passport_input_str:
        this_passport_valid = False
    if "iyr" not in passport_input_str:
        this_passport_valid = False
    if "eyr" not in passport_input_str:
        this_passport_valid = False
    if "hgt" not in passport_input_str:
        this_passport_valid = False
    if "hcl" not in passport_input_str:
        this_passport_valid = False
    if "ecl" not in passport_input_str:
        this_passport_valid = False
    if "pid" not in passport_input_str:
        this_passport_valid = False
    return this_passport_valid


def byr(byr) -> bool:
    """
    Given a value associated with a "byr" entry in a passport
    return True if matches schema:
    byr (Birth Year) - four digits; at least 1920 and at most 2002
    """
    valid = True
    if len(str(byr)) != 4:
        valid = False
    if int(byr) > 2002:
        valid = False
    if int(byr) < 1920:
        valid = False
    return valid


def iyr(iyr) -> bool:
    """
    Given a value associated with a "iyr" entry in a passport
    return True if matches schema:
    iyr (Issue Year) - four digits; at least 2010 and at most 2020
    """
    valid = True
    if len(str(iyr)) != 4:
        valid = False
    if int(iyr) > 2020:
        valid = False
    if int(iyr) < 2010:
        valid = False
    return valid


def eyr(eyr) -> bool:
    """
    Given a value associated with a "eyr" entry in a passport
    return True if matches schema:
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030
    """
    valid = True
    if len(str(eyr)) != 4:
        valid = False
    if int(eyr) > 2030:
        valid = False
    if int(eyr) < 2020:
        valid = False
    return valid


def hgt(hgt) -> bool:
    """
    Given a value associated with a "hgt" entry in a passport
    return True if matches schema:
    hgt (Height) - a number followed by either cm or in:
    - If cm, the number must be at least 150 and at most 193
    - If in, the number must be at least 59 and at most 76
    """
    valid = False
    if hgt[-2:] == "cm":
        hgt_int = int(hgt[:-2])
        if (hgt_int <= 193) and (hgt_int >= 150):
            valid = True
    if hgt[-2:] == "in":
        hgt_int = int(hgt[:-2])
        if (hgt_int <= 76) and (hgt_int >= 59):
            valid = True
    return valid


def hcl(hcl, passport_full_str) -> bool:
    """
    Given a value associated with a "hcl" entry in a passport
    return True if matches schema:
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f
    """
    valid = False
    pattern = re.compile("^([0-9a-z]{6})$")
    if "hcl:#" in passport_full_str:
        match = re.match(pattern, hcl)
        if match:
            valid = True
    return valid


def ecl(ecl) -> bool:
    """
    Given a value associated with a "ecl" entry in a passport
    return True if matches schema:
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth
    """
    valid = False
    if "amb" in ecl:
        valid = True
    if "blu" in ecl:
        valid = True
    if "brn" in ecl:
        valid = True
    if "gry" in ecl:
        valid = True
    if "grn" in ecl:
        valid = True
    if "hzl" in ecl:
        valid = True
    if "oth" in ecl:
        valid = True
    return valid


def pid(pid) -> bool:
    """
    Given a value associated with a "pid" entry in a passport
    return True if matches schema:
    pid (Passport ID) - a nine-digit number, including leading zeroes
    """
    valid = False
    pattern = re.compile("^([0-9]{9})$")
    match = re.match(pattern, pid)
    if match:
        valid = True
    return valid


def cid(cid):
    """
    Given a value associated with a "pid" entry in a passport
    return True whatever happens:
    cid (Country ID) - ignored, missing or not.
    """
    return True


def valid_against_schema(passport_list, passport_str):
    valid = True
    while len(passport_list) > 0:
        # Grab the first two keys
        key = passport_list.pop(0)
        val = passport_list.pop(0)

        # Lookup the method we will use
        method = possibles.get(key)
        if not method:
            raise NotImplementedError("Method %s not implemented" % key)

        # Evaluate the value associated with the key
        if key == "hcl":
            if not method(val, passport_str):
                # print("Failed to match on func", key)
                valid = False
        else:
            if not method(val):
                # print("Failed to match on func", key)
                valid = False
    return valid


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST_P1 = Path.cwd() / "./data/day4_input_test_part1.txt"
    INPUT_PATH_TEST_P2 = Path.cwd() / "./data/day4_input_test_part2.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day4_input.txt"

    # Read the files and convert to lists of strings
    passports_test_p1 = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST_P1, "\n\n")
    passports_test_p2 = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST_P2, "\n\n")
    passports_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_REAL, "\n\n")

    # Part 1
    # Input data contains passport like data separated by blank lines
    # Passports contain 8 fields:
    # - byr (Birth Year)
    # - iyr (Issue Year)
    # - eyr (Expiration Year)
    # - hgt (Height)
    # - hcl (Hair Color)
    # - ecl (Eye Color)
    # - pid (Passport ID)
    # - cid (Country ID)
    # Passports are valid if they have all fields, or are ONLY missing CID
    # Answer: How many valid Passports?
    num_valid_passports = 0
    for passport in passports_test_p1:
        if this_passport_valid(passport):
            num_valid_passports += 1
    print("Part 1 - Test - Num valid passports:", num_valid_passports)

    num_valid_passports = 0
    for passport in passports_real:
        if this_passport_valid(passport):
            num_valid_passports += 1
    print("Part 1 - Real - Num valid passports:", num_valid_passports)

    # Part 2
    # Ignore the cid field. Every other field has rules defining valid values:
    # Answer: num passports with all required fields and valid values.

    # Get all the function names in this file
    possibles = globals().copy()
    possibles.update(locals())

    num_valid_passports = 0
    for entry in passports_test_p2:
        # Begin by testing if a password met the critera of part 1
        if this_passport_valid(entry):
            # Split into a list of keys and values
            entry_as_list = re.split(r"\W+", entry)
            if valid_against_schema(entry_as_list, entry):
                num_valid_passports += 1
    print("Part 2 - Test - Num valid passports:", num_valid_passports)

    num_valid_passports = 0
    for entry in passports_real:
        # Begin by testing if a password met the critera of part 1
        if this_passport_valid(entry):
            # Split into a list of keys and values
            entry_as_list = re.split(r"\W+", entry)
            if valid_against_schema(entry_as_list, entry):
                num_valid_passports += 1
    print("Part 2 - Real - Num valid passports:", num_valid_passports)
