# -*- coding: utf-8 -*-
def read_file_as_str(file_path) -> str:
    """
    Open a file as read only and return as a string.
    Strips any whitespace from either end of the string.
    """
    assert file_path.exists()
    with open(file_path, "r") as input_file:
        input_Str = input_file.read().strip()
    return input_Str


def str_to_list_of_str(input_str, split_char) -> [str]:
    """
    Return a list of strings, new string each time split_char is seen.
    """
    return [x for x in input_str.split(split_char)]


def list_of_str_to_list_of_int(list_of_strings) -> [int]:
    """
    Accepts a list of strings, returns a list of ints.
    The map() function executes a specified function for each item in an iterable.
    """
    return list(map(int, list_of_strings))


def read_file_as_list_of_str(file_path, delimiter) -> [str]:
    """
    Open a file as read only and return as a string.
    Strips any whitespace from either end of the string.
    Return a list of strings, new string each time delimiter is seen.
    """
    return str_to_list_of_str(read_file_as_str(file_path), delimiter)


def read_file_as_list_of_int(file_path, delimiter) -> [str]:
    """
    Open a file as read only and return as a string.
    Strips any whitespace from either end of the string.
    Converts to a list of strings, new string each time delimiter is seen.
    Converts the list of strings to a list of ints, and returns this.
    """
    puzzle_input = str_to_list_of_str(read_file_as_str(file_path), delimiter)
    return list_of_str_to_list_of_int(puzzle_input)
