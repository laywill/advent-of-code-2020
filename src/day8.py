#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


class opcode_computer(object):
    """
    A class to be an opcode computer
    """

    def __init__(self, input_path) -> None:
        """
        docstring
        """
        # Import program from file
        self.program = aoc_file_io.read_file_as_list_of_str(input_path, "\n")

        for i in range(len(self.program)):
            # Store each line as list of strings
            self.program[i] = self.program[i].split()

            # Convert value provided to integer
            self.program[i][1] = int(self.program[i][1])

            # Add a third entry to record if an opcode has been executed
            self.program[i].append(0)

            # Refactor the list into a Dictionary so we aren't
            #   working with arbitrary indexes.
            self.program[i] = {
                "opcode": self.program[i][0],
                "value": self.program[i][1],
                "executed": self.program[i][2],
            }

        self.program_length = len(self.program)

        # Create a Program Counter
        self.pc = 0

        # Accumulator is increased or decreased by acc opcode
        self.accumulator = 0

    def _acc(self, value) -> None:
        """
        Perform an ACC opcode
        """
        self.accumulator += value
        self.program[self.pc]["executed"] += 1
        self.pc += 1

    def _jmp(self, value) -> None:
        """
        Perform a JMP opcode to jump the Program Counter to a new position.
        """
        self.program[self.pc]["executed"] += 1
        self.pc += value

    def _nop(self, value) -> None:
        """
        Perform a No Operationopcode.
        """
        self.program[self.pc]["executed"] += 1
        self.pc += 1

    def _run_opcode(self) -> None:
        """
        Runs a single opcode selected by the current value of self.pc
        """
        if self.program[self.pc]["opcode"] == "acc":
            self._acc(self.program[self.pc]["value"])
        elif self.program[self.pc]["opcode"] == "jmp":
            self._jmp(self.program[self.pc]["value"])
        elif self.program[self.pc]["opcode"] == "nop":
            self._nop(self.program[self.pc]["value"])
        else:
            raise NotImplementedError(
                "Method %s not implemented" % self.program[self.pc]["opcode"]
            )

    def modify_opcode(self, index) -> bool:
        """
        Modify an opcode at the given index.
          NOP ==> JMP
          JMP ==> NOP
        Return true if successfully modified.
        Return false if could not modify (e.g if ACC found)
        """
        if self.program[index]["opcode"] == "acc":
            return False
        elif self.program[index]["opcode"] == "jmp":
            self.program[index]["opcode"] = "nop"
            return True
        elif self.program[index]["opcode"] == "nop":
            self.program[index]["opcode"] = "jmp"
            return True
        else:
            raise ValueError("Opcode at Line", index, "not recognised.")

    def run_program_part_1(self) -> int:
        """
        Runs a program, stops when about to run an instruction for the second time.
        Returns the value of the accumulator.
        """
        keep_running = True
        while keep_running:
            # Stop executing if we are about to execute an Opcode for a second time
            if self.program[self.pc]["executed"] > 0:
                break
            else:
                self._run_opcode()
        return self.accumulator

    def run_program_part_2(self, stop_threshold) -> int:
        """
        Runs a program, stops after running the last instruction in the program.
        Returns the value of the accumulator.
        """
        keep_running = True
        while keep_running:
            if self.pc == self.program_length:
                # Return at last!
                return self.accumulator
            else:
                if self.program[self.pc]["executed"] > stop_threshold:
                    # Break out of the loop and return invalid
                    return None
                else:
                    self._run_opcode()


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day8_input_test.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day8_input.txt"

    # Part 1
    # Answer: Accumulator value just BEFORE running an opcode for second time
    computer = opcode_computer(INPUT_PATH_TEST)
    print("Part 1 - Test - Accumulator:", computer.run_program_part_1())

    computer = opcode_computer(INPUT_PATH_REAL)
    print("Part 2 - Real - Accumulator:", computer.run_program_part_1())

    # Part 2
    # Answer: Accumulator value when program exits by executing the last
    #           instruction in the program.
    # There is only one opcode which will complete in this manner
    stop_after_an_opcode_used_this_many_times = 100

    computer = opcode_computer(INPUT_PATH_TEST)
    num_ops = computer.program_length
    for i in range(num_ops):
        computer = opcode_computer(INPUT_PATH_TEST)
        if computer.modify_opcode(i):
            result = computer.run_program_part_2(
                stop_after_an_opcode_used_this_many_times
            )
            if result:
                print("Part 2 - Test - Accumulator:", result)
                break
        else:
            # We could not modify an opcode, so don't run this
            pass

    computer = opcode_computer(INPUT_PATH_REAL)
    num_ops = computer.program_length
    for i in range(num_ops):
        computer = opcode_computer(INPUT_PATH_REAL)
        if computer.modify_opcode(i):
            result = computer.run_program_part_2(
                stop_after_an_opcode_used_this_many_times
            )
            if result:
                print("Part 2 - Real - Accumulator:", result)
                break
        else:
            # We could not modify an opcode, so don't run this
            pass
