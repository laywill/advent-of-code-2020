#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


def rules_to_lookup(rules_list) -> dict:
    """
    Takes in a list of rules (strings).
    Parses the strings to return a lookup for how many bags of each colour a bag can contain.

    Returns a lookup dictionary, where keys are bag colours,
        Values are a dictionary, which in turn contains:
            bag colours as keys, num bags as value
    """
    lookup = {}
    for i in range(len(rules_list)):
        # Break the rule into the containing bag and which, if any,
        # bags it contains
        container, contents = rules_list[i].split(" bags contain ")

        # Clean the strings for further parsing
        contents = contents.replace(".", "")
        contents = contents.replace(",", "")
        contents = contents.replace("bags", "bag")
        contents = contents.strip()

        # Split the contents into a list of contents
        # For some reason, splitting on " bag " solves a problem where, if we
        #   split on " bag" the final entry in the string is "" which we need
        #   to drop, but many strings have preceeding whitespace.
        # This method fixes the preceeding whitespace, but we have to slice
        #  " bag" from the end of the final list entry. Oh well...
        contents = contents.split(" bag ")
        contents[-1] = contents[-1][:-4]

        # Create a dictionary of the contents
        #   Keys = bag colours
        #   Values = number of bags of each colour
        contents_dict = {}
        for x in contents:
            if x == "no other":
                pass
            else:
                # Assume only 1 - 9 bags can be specified
                # Slice description and use as key. Store number as value.
                contents_dict[x[2:]] = int(x[0])

        # Commit contents dictionary to lookup dictionary, using containing
        #   bag colour as the key
        lookup[container] = contents_dict
    return lookup


def bag_colour_lookup(rules_dict, look_in_bag_colour, look_for_bag_colour) -> bool:
    """
    Return true if one of the bags inside this bag is the colour we want
    """
    bag_found = False
    # Empty dictionaries return false. These are empty bags!
    if not rules_dict[look_in_bag_colour]:
        pass
    else:
        now_look_in = list(rules_dict[look_in_bag_colour].keys())
        if look_for_bag_colour in now_look_in:
            bag_found = True
        else:
            for bag in now_look_in:
                recursive_bag_found = bag_colour_lookup(
                    rules_dict, bag, look_for_bag_colour
                )
                if recursive_bag_found:
                    bag_found = True
                    break
    return bag_found


def bag_number_lookup(rules_dict, look_in_bag_colour) -> int:
    """
    Look in a bag of a particular colour, and find out how many bags it contains.
    """
    num_bags = 0
    # Empty dictionaries return false. These are empty bags!
    if not rules_dict[look_in_bag_colour]:
        pass
    else:
        now_look_in = list(rules_dict[look_in_bag_colour].keys())
        for bag in now_look_in:
            num_this_bag = rules_dict[look_in_bag_colour][bag]
            num_bags_inside = bag_number_lookup(rules_dict, bag)
            num_bags += num_this_bag + (num_this_bag * num_bags_inside)
    return num_bags


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST_P1 = Path.cwd() / "./data/day7_input_test_part1.txt"
    INPUT_PATH_TEST_P2 = Path.cwd() / "./data/day7_input_test_part2.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day7_input.txt"

    # Read the files and convert to lists of strings
    rules_test_p1 = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST_P1, "\n")
    rules_test_p2 = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST_P2, "\n")
    rules_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_REAL, "\n")

    # Split into bag, and containing bag
    rules_test_p1 = rules_to_lookup(rules_test_p1)
    rules_test_p2 = rules_to_lookup(rules_test_p2)
    rules_real = rules_to_lookup(rules_real)

    # Part 1
    # Answer: How many colors can, eventually, contain at least one shiny gold bag?
    bags_containing_gold = 0
    for rule in rules_test_p1:
        if bag_colour_lookup(rules_test_p1, rule, "shiny gold"):
            bags_containing_gold += 1
    print("Part 1 - Test - Bags containing gold:", bags_containing_gold)

    bags_containing_gold = 0
    for rule in rules_real:
        if bag_colour_lookup(rules_real, rule, "shiny gold"):
            bags_containing_gold += 1
    print("Part 1 - Real - Bags containing gold:", bags_containing_gold)

    # Part 2
    # How many individual bags are required inside your single shiny gold bag?
    bags_in_gold = bag_number_lookup(rules_test_p2, "shiny gold")
    print("Part 2 - Test - Bags in gold:", bags_in_gold)

    bags_in_gold = bag_number_lookup(rules_real, "shiny gold")
    print("Part 2 - Real - Bags in gold:", bags_in_gold)
