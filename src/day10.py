#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


def order_adapters(adapters_list) -> [int]:
    """
    From a list of adapters, order them such that the adapter with closest
    value to input follows it. Adapters always step joltage UP.
    This can be simplified to sorting the list.
    Return sorted list of ints.
    """
    adapters_list.sort()
    return adapters_list


def count_differences(ordered_adapters_list, difference) -> int:
    """
    Count the number of intervals in ORDERED_ADAPTERS_LIST which have
    the difference DIFFERENCE.
    e.g. [1,2,4,6,7] has 2 intervals of difference 1, and 2 of 2
    """
    count = 0
    for i in range(len(ordered_adapters_list) - 1):
        interval = ordered_adapters_list[i + 1] - ordered_adapters_list[i]
        if interval == difference:
            count += 1
        else:
            pass
    print("Counted", count, "differences of", difference, "jolt")
    return count


def count_arrangements(ordered_adapters_list) -> int:
    """
    From ordered_adapters_list, find the number of ways they can connect.
    I had no idea how to approach Part 2.
    While searching for an alogithm I found this, and once seen I could not
    unsee such an elegant solution as this:
    https://www.reddit.com/r/adventofcode/comments/ka8z8x/2020_day_10_solutions/gf9mvrh/
    """
    # Create a dictionary to store the number of possible routes "to each joltage".
    routes = {}

    # Initialise with 1 route to the starting joltage.
    # We can take this shortcut, becasue we pre-pended 0 to all inputs
    routes[0] = 1

    # Begin iterating through adaptors
    #   (ignoring the first value because we already set it above).
    for j in ordered_adapters_list[1:]:
        # Each joltage route is equal to the sum of the number of routes to
        #   the previous three joltages. However, some of the joltages won't
        #   be present in the list of adaptors.
        # So the number of routes to them will be 0.
        routes[j] = routes.get(j - 1, 0) + routes.get(j - 2, 0) + routes.get(j - 3, 0)

    # Return the number of possible routes to get to the final adaptor.
    return routes[max(routes.keys())]


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST_SHORT = Path.cwd() / "./data/day10_input_test_short.txt"
    INPUT_PATH_TEST_LONG = Path.cwd() / "./data/day10_input_test_long.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day10_input.txt"

    # Read the files and convert to lists of ints
    input_test_short = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST_SHORT, "\n")
    input_test_long = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST_LONG, "\n")
    input_real = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_REAL, "\n")

    # Part 1
    # Find a chain that uses all of your adapters to connect the charging
    # outlet to your device's built-in adapter and count the joltage
    # differences between the charging outlet, the adapters, and your device.
    # Answer: What is the number of 1-jolt differences multiplied by the
    #           number of 3-jolt differences?

    # Part 2
    # Answer: What is the total number of distinct ways you can arrange the
    #           adapters to connect the charging outlet to your device?

    # Treat the charging outlet near your seat as having an effective joltage rating of 0.
    joltage = 0

    # TEST-SHORT
    print("\nTest - Short")
    # Device can cope with a joltage 3 higher than highest adapter output
    device_joltage_max = max(input_test_short) + 3
    adapters = order_adapters(input_test_short)
    adapters.insert(0, joltage)
    adapters.append(device_joltage_max)
    num_1jolt = count_differences(adapters, 1)
    num_3jolt = count_differences(adapters, 3)
    print(num_1jolt, "differences of 1 jolt\t", num_3jolt, "differences of 3 jolts")
    answer = num_1jolt * num_3jolt
    print("Part 1 - Test Short - Answer:", answer)
    arrangements = count_arrangements(adapters)
    print("Part 2 - Test Short - Answer:", arrangements)

    # TEST-LONG
    print("\nTest - Long")
    # Device can cope with a joltage 3 higher than highest adapter output
    device_joltage_max = max(input_test_long) + 3
    adapters = order_adapters(input_test_long)
    adapters.insert(0, joltage)
    adapters.append(device_joltage_max)
    num_1jolt = count_differences(adapters, 1)
    num_3jolt = count_differences(adapters, 3)
    print(num_1jolt, "differences of 1 jolt\t", num_3jolt, "differences of 3 jolts")
    answer = num_1jolt * num_3jolt
    print("Part 1 - Test Long - Answer:", answer)
    arrangements = count_arrangements(adapters)
    print("Part 2 - Test Long - Answer:", arrangements)

    # REAL
    print("\nReal")
    # Device can cope with a joltage 3 higher than highest adapter output
    device_joltage_max = max(input_real) + 3
    adapters = order_adapters(input_real)
    adapters.insert(0, joltage)
    adapters.append(device_joltage_max)
    num_1jolt = count_differences(adapters, 1)
    num_3jolt = count_differences(adapters, 3)
    print(num_1jolt, "differences of 1 jolt\t", num_3jolt, "differences of 3 jolts")
    answer = num_1jolt * num_3jolt
    print("Part 1 - Real - Answer:", answer)
    arrangements = count_arrangements(adapters)
    print("Part 2 - Real - Answer:", arrangements)
