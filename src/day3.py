#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io
import numpy as np


class piste(object):
    """
    A class to represent the piste input provided
    """

    def __init__(self, input_path):
        # Read the files and convert to lists of strings
        self.piste = aoc_file_io.read_file_as_list_of_str(input_path, "\n")

        # Get piste dimensions
        self.height = len(self.piste)
        self.width = len(self.piste[0])

        # Convert the lists of strings into lists of lists of characters
        for i in range(len(self.piste)):
            self.piste[i] = [char for char in self.piste[i]]

        self.current_loc = {"x": 0, "y": 0}

    def move_r3_d1(self, step_x, step_y):
        # Make a move...
        self.current_loc["x"] += step_x
        self.current_loc["y"] += step_y

        # Update actual location as the piste wraps
        self.current_loc["x"] = self.current_loc["x"] % self.width

        # print(self.current_loc)
        # See if we hit a tree, if so return 1
        char = self.piste[self.current_loc["y"]][self.current_loc["x"]]
        if char == "#":
            return 1

        # If not... return 0
        return 0


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day3_input_test.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day3_input.txt"

    # Part 1
    # The toboggan can only follow a few specific slopes, moving right 3 and
    # down 1 each step it takes.
    # - Count all the trees you would encounter starting at X = 0, Y = 0 and
    #   continuing until you reach the bottom of the piste
    test_piste = piste(INPUT_PATH_TEST)
    print("Current location:", test_piste.current_loc)
    how_many_trees = 0
    while test_piste.current_loc["y"] < test_piste.height - 1:
        how_many_trees += test_piste.move_r3_d1(3, 1)
    print("Part 1 - Test: We hit this many trees:", how_many_trees)

    real_piste = piste(INPUT_PATH_REAL)
    print("Current location:", real_piste.current_loc)
    how_many_trees = 0
    while real_piste.current_loc["y"] < real_piste.height - 1:
        how_many_trees += real_piste.move_r3_d1(3, 1)
    print("Part 1 - Real: We hit this many trees:", how_many_trees)

    # Part 2
    # The toboggan can now follow any path we want, but we will test it with:
    # - Right 1, down 1.
    # - Right 3, down 1. (This is the slope we already checked.)
    # - Right 5, down 1.
    # - Right 7, down 1.
    # - Right 1, down 2.
    # Multiply the result for all of these together to get the answer
    step_paterns_xy = [[1, 1], [3, 1], [5, 1], [7, 1], [1, 2]]

    trees = []
    test_piste = piste(INPUT_PATH_TEST)
    for xy in step_paterns_xy:
        test_piste.current_loc["x"] = 0
        test_piste.current_loc["y"] = 0
        how_many_trees = 0
        while test_piste.current_loc["y"] < test_piste.height - 1:
            how_many_trees += test_piste.move_r3_d1(xy[0], xy[1])
        trees.append(how_many_trees)

    # using np.prod() to get the multiplication of all elements in the list
    multiplied_trees = np.uint(np.prod(trees))
    print("Part 2 - Test: We hit this many trees:", multiplied_trees)

    trees = []
    real_piste = piste(INPUT_PATH_REAL)
    for xy in step_paterns_xy:
        real_piste.current_loc["x"] = 0
        real_piste.current_loc["y"] = 0
        how_many_trees = 0
        while real_piste.current_loc["y"] < real_piste.height - 1:
            how_many_trees += real_piste.move_r3_d1(xy[0], xy[1])
        trees.append(how_many_trees)

    print(trees)

    # using np.prod() to get the multiplication of all elements in the list
    # This could be a big number so store as a numpy.uint
    multiplied_trees = np.uint(np.prod(trees))
    print("Part 2 - Real: We hit this many trees:", multiplied_trees)
