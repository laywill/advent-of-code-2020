#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


def check_a_number(cyper, index, look_behind_length) -> bool:
    """
    Returns true, if the value at index can be made by adding together two
    numbers from the values contained in the previous N entries, where N is
    the look_behind_length.
    Returns false otherwise.
    """
    valid_value = False
    # i and j will itterate 0, 1, 2 .... look_behind_length - 1
    for i in range(look_behind_length):
        for j in range(look_behind_length):
            if i == j:
                # Cannot use the same number twice
                pass
            else:
                if (
                    cyper[index]
                    == cyper[index - look_behind_length + i]
                    + cyper[index - look_behind_length + j]
                ):
                    valid_value = True
                    break
                else:
                    # These two integers didn't add up to the target number
                    pass
    return valid_value


def find_contiguous_components(cypher, target) -> [int]:
    """
    Given the cypher, and the target value, find a list of adjacent values in the
    cypher that sum to the value of TARGET.
    Return the list of values
    """
    # Initialise some indexes
    i_lo = 0
    i_hi = i_lo + 1
    solved = False
    while not solved:
        if sum(cypher[i_lo:i_hi]) < target:
            # We added some numbers up, but didn't reach target
            # increase size of slice and try again
            i_hi += 1
        elif sum(cypher[i_lo:i_hi]) == target:
            # We added up some numbers and exactly found the target value
            # Stop searching
            break
        else:
            # Implied: sum(cypher[i_lo:i_hi]) > target
            # We need to look again, but starting with a new i_lo
            i_lo += 1
            i_hi = i_lo + 1
    return cypher[i_lo:i_hi]


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day9_input_test.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day9_input.txt"

    # Read the files and convert to lists of ints
    cypher_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, "\n")
    cypher_real = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_REAL, "\n")

    # Part 1
    # Find the first number in the list (after the preamble, length N)
    #   which is not the sum of two of the N numbers before it.
    # Answer: The first number that does not have this property.

    # Part 2
    # find a contiguous set of at least two numbers in your list which sum to
    #   the invalid number from step 1.
    # Answer: add together the smallest and largest number in this contiguous range

    # Set the preamble length
    preamble_length_test = 5
    preamble_length_real = 25

    # Initialise an index pointer
    index_test = preamble_length_test
    index_real = preamble_length_real

    # TEST
    # Part 1
    while True:
        index_valid = check_a_number(cypher_test, index_test, preamble_length_test)
        if not index_valid:
            break
        else:
            index_test += 1
    print("Part 1 - Test - First number to not have property", cypher_test[index_test])
    # Part 2
    components = find_contiguous_components(cypher_test, cypher_test[index_test])
    encryption_weakness = min(components) + max(components)
    print("Part 2 - Test - Encryption weaknes:", encryption_weakness)

    # REAL
    # Part 1
    while True:
        index_valid = check_a_number(cypher_real, index_real, preamble_length_real)
        if not index_valid:
            break
        else:
            index_real += 1
    print("Part 1 - Real - First number to not have property:", cypher_real[index_real])
    # Part 2
    components = find_contiguous_components(cypher_real, cypher_real[index_real])
    encryption_weakness = min(components) + max(components)
    print("Part 2 - Real - Encryption weaknes:", encryption_weakness)
