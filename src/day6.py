#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from pathlib import Path

import aoc_file_io


def num_unique_chars_in_str(responses) -> int:
    """
    Takes a string, finds the number of unique characters that are in it
    """
    responses = responses.replace(" ", "")
    responses = responses.replace("\n", "")
    list_of_chars = responses.split()
    return len(set(list(list_of_chars[0])))


def num_unique_chars_in_list_of_strs(list_of_strs) -> int:
    """
    Takes a list of strings, finds the number of unique characters which are
    in every string in the list.
    """
    # The map() function executes a specified function for each item in an iterable.
    # Here we create a set for each entry in list_of_strs to remove duplicates.
    # We store the result as a tuple because this data is immutable
    chars = tuple(map(set, list_of_strs))

    # Sets can use set_name.intersection(other_set) to return the elements of
    #   set_name and other_set that appear in both.
    # For sequences such as string, list and tuple,  * is a repetition operator when
    #   placed before the variable name.
    # By passing *chars we indicate that we want the intersetion between the first set
    #   and all sets in the tuple.
    chars_in_all_responses = len(chars[0].intersection(*chars))

    return chars_in_all_responses


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day6_input_test.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day6_input.txt"

    # Read the files and convert to lists of strings
    responses_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST, "\n\n")
    responses_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_REAL, "\n\n")

    # Part 1
    # For each group of strings find total number of unique characters
    # Answer: Sum of the totals
    responses = 0
    for group in responses_test:
        responses += num_unique_chars_in_str(group)
    print("Part 1 - Test - Sum of unique responses:", responses)

    responses = 0
    for group in responses_real:
        responses += num_unique_chars_in_str(group)
    print("Part 1 - Real - Sum of unique responses:", responses)

    # Part 2
    # For each group of strings, find total number of responses where everyone
    #   selected a particular answer.
    # Answer: Sum of the totals
    responses = 0
    for group in responses_test:
        responses += num_unique_chars_in_list_of_strs(group.split("\n"))
    print("Part 2 - Test - Sum of consensus responses:", responses)

    responses = 0
    for group in responses_real:
        responses += num_unique_chars_in_list_of_strs(group.split("\n"))
    print("Part 2 - Real - Sum of consensus responses:", responses)
