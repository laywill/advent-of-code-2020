#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-
from math import ceil
from math import floor
from pathlib import Path

import aoc_file_io
import numpy as np


def find_row_or_col(fb_str, total) -> int:
    """
    Takes a string of characters.
    Returns a row given by doing binary space partitioning.
    """
    min_rc = 0
    max_rc = total - 1
    chars = list(fb_str)
    for ch in chars:
        if (ch == "F") or (ch == "L"):
            max_rc = floor(min_rc + ((max_rc - min_rc) / 2))
        elif (ch == "B") or (ch == "R"):
            min_rc = ceil(min_rc + ((max_rc - min_rc) / 2))
    assert min_rc == max_rc
    return max_rc


def seat_id(row, col) -> int:
    """
    Takes row and column integers, and returns seat_ID number
    """
    return (8 * row) + col


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day5_input_test.txt"
    INPUT_PATH_REAL = Path.cwd() / "./data/day5_input.txt"

    # Read the files and convert to lists of strings
    passes_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST, "\n")
    passes_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_REAL, "\n")

    # Part 1
    # Boarding passes use binary space partitioning to seat people.
    # Plane has 128 rows (0 to 127)
    # Plane has 8 columns (0 to 7)
    # FIRST 7 characters are "F" or "B", to specify a row
    # LAST 3 characters are "L" or "R", to specify column
    # Seat ID = (row * 8) + column
    # Answer: highest seat ID on a boarding pass
    num_rows = 128
    num_cols = 8

    seat_ids = []
    for entry in passes_test:
        row = find_row_or_col(entry[:7], num_rows)
        col = find_row_or_col(entry[-3:], num_cols)
        seat_ids.append(seat_id(row, col))
    print("Part 1 - Test - Answer:", max(seat_ids))

    seat_ids = []
    for entry in passes_real:
        row = find_row_or_col(entry[:7], num_rows)
        col = find_row_or_col(entry[-3:], num_cols)
        seat_ids.append(seat_id(row, col))
    print("Part 1 - Real - Answer:", max(seat_ids))

    # Part 2
    # It's a full flight, your seat should be the only missing pass in the list.
    # However, some seats at the front and back of the plane don't exist,
    #  so they'll be missing from your list as well.
    # Your seat wasn't at the very front or back.
    #  the seats with IDs +1 and -1 from yours will be in your list.
    # Answer: What is the ID of your seat?
    my_seat = []
    for x in range(num_cols):
        for y in range(1, num_rows - 1):
            # Calculate the Seat ID for this seat
            # If it is not already taken, evaluate...
            try_seat = seat_id(y, x)
            if try_seat not in seat_ids:
                if (try_seat - 1) in seat_ids:
                    if (try_seat + 1) in seat_ids:
                        # This is our seat!
                        my_seat.append(try_seat)
                    else:
                        # This seat isn't taken!
                        pass
                else:
                    # This seat isn't taken!
                    pass
            else:
                # This seat is alrady taken!
                pass
    assert len(my_seat) == 1
    print("Part 2 - Real - Answer:", my_seat[0])
